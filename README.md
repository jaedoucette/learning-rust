# learning-rust

Working through "The Rust Programming Language".

## Chapter 6 Notes

- Rust's enum construct seems extremely powerful, particularly the ability to group several different data types under a single header in a sensible way. It looks a lot like C's union construct, but done right. 

 - Something that's not clear from the book is what the cannonical practice is for declaring enums. It seems like there's a lot of synatax available. For instance, I could declare an enum over a set of anonymously defined structs. It seems like this might be more readable (it's very compact, and well localized), but might also be harder to maintain (what if later I want to add methods to the struct?). On second thought, Maybe it would be quite easy to change the definition of
   the struct to a non-anonymous one.

 - The ability to define methods over enums seems a lot like the concept of an interface in Java, except that it's also immediately obvious which classes implement the interface (since they're all listed out in the enum, rather than appending the interface after each class's declaration). Pretty neat.

 - Optional, where have you been all my life. It seems like such an obvious idea in retrospect: of course a null reference is fundamentally a different datatype than a non-null reference: it doesn't implement any of the same operations! Of course you should be able to  detect that type conflict at compile time! Really drives home the "C is like playing with a loaded gun" metaphor. 

 - It looks like the Optional class also supports lazy evaluation based on return types in a syntatically nice way. So you can write something like: mymethod().unwrap\_or("value for when mymethod() produces None").doSomethingElse().

## Chapter 4 Notes

- Rust has a novel (unique?) model for memory management based on the idea of "ownership". Heap-allocated values are owned by the variable that they are initially stored in. If the variable is passed by value, or used as the RHS of an assignment statement, then the receiving variable (i.e. parameter or LHS) takes ownership, and the original variable can no longer be used to access that data. This prevents dangling references, and also prevents double-frees, since attempts to do either of these are detectable at compile-time.

- To free data, Rust simply inserts a call to a special function ("drop") whenever the variable owning that data leaves scope. This prevents memory leaks (since variables are automatically freed). 

- References to a variable can be made explicitly, using the famaliar & notation. A reference cannot be used to free the variable however, because it does not own it. 

- References are also immutable by default, and even more interestingly, only a single mutable reference to a given piece of data can exist within a given scope (i.e. no mutable aliases can be made). This seems like it would make it much easier to reason about the behaviour of functions, and potentially even multi-threaded code.

- Indeed, the book claims compile-time detection of race conditions. To ensure this, it also requires that no immutable references are in-scope when a mutable reference is created (and vice-versa). This seems to require developers to ensure that there's only one view of the data when mutation occurs, which is super sensible, but couldn't a race condition still occur if my mutable reference changes data at the same time as I change the value of the data itself?

- Slices seem super handy, but will take some getting used to. The key argument for their use (instead of indices into an array) is that they are references into a region of memory that the compiler can track. Using only slices should prevent a lot of run-time bugs, especially the kind that cause data corruption.


## Chapter 3 Notes

- The default immutablity of variables differs from const in that an immutable 
  variable can be repurposed ("shadowed") as a new variable of the same name, 
  potentially of a different type and value, while a constant has a constant type
  and can never change. This is a nice way to get around the problem of type
  conversion and data parsing when working with immutable variables. Constants 
  also must be set to compile-time evaluatable expressions.

- Rust scopes, denoted by {}, function essentially as local scopes in functional
  languages like Scheme. They contain a list of statements, optionally followed
  by an expression. The return value of a scope is the value of the terminating
  expression. In the absence of a terminal expression, the scope resolves to
  _no_ return value at all, and cannot be used in assignments.

- No multiline comments in Rust? Looks like there's a documenation-style comment
  explained later...

- Conditionals do not support compact syntax. Each arm must be placed in a new
  scope, even if it contains only a single statement. It looks like Match is
  intended to be used for any complex conditional though, and this syntax 
  requirement is meant to nudge you in that direction.

- Conditional expressions can be used in assignment statements, but only if 
  every arm resolves to the same type. This seems reasonable coming from a 
  strong-typing background, but it sort of breaks the abstraction of Rust's 
  type inference engine, because the compiler can't determine which arm will 
  be used, even if all the necessary information is present at compile-time.

- Rust has ranges (probably discussed more later) using the notation (start..end)
  these eleminate the need for most instances of while.
