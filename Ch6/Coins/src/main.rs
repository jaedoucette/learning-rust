#[derive(Debug)]
enum CapType {
    Cola,
    SevenUp,
    RootBeer,
    Milk
}

#[derive(Debug)]
enum Coin {
    Dubloon,
    ElectrumPiece,
    HalfDollar,
    BottleCap(CapType),
}


impl Coin {
    fn value(&self) -> String {
        let res = match self {
           Coin::Dubloon => "8 pieces of 8.",
           Coin::ElectrumPiece => "No one remembers.",
           Coin::HalfDollar => "0.5 Dollars.",
           Coin::BottleCap(the_type) => 
               match the_type {
                   CapType::Cola => "1/8 a pre-war money.",
                   CapType::SevenUp => "Seven.",
                   CapType::RootBeer => "$22.",
                   CapType::Milk => "Extra Bone."
               },
            
        };
        String::from(res)
    }
}

fn main() {
    let c = Coin::Dubloon;

    println!("A {:?} is worth {}", c, c.value()); 

    let coin2 = Coin::BottleCap(CapType::Milk);
    println!("A {:?} is worth {}", coin2, coin2.value()); 

}
