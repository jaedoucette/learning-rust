
struct Flight {
    aircraft : Option<String>,
    destination : String,
    origin : String,
}

impl Flight {
    fn display(&self) {
        match &self.aircraft {
            None => println!("Aircraft not yet ready."),
            Some(i) => println!("From {} to {} in a {}.",
                                self.origin, self.destination, i),
        }
    }
}


fn main(){
    let mut my_flight = Flight { aircraft: None, 
        destination : String::from("YTZ"), 
        origin : String::from("LGA")};
    my_flight.display();

    my_flight.aircraft = Some(String::from("Boeing 737-300"));
    my_flight.display();
}
