#[derive(Debug)]
struct Point(i32, i32, i32);

impl Point {
    fn euclidian_dist(&self, p2: &Point) -> f64 {
        let dx = self.0 - p2.0;
        let dy = self.1 - p2.1;
        let dz = self.2 - p2.2;
        let dist = (dx * dx + dy * dy + dz * dz) as f64;
        dist.sqrt()
    }
}

#[derive(Debug)]
struct Rect(Point, Point);

impl Rect {
    fn area(&self) -> u32 {
        let width = ((self.0).0 - (self.1).0).abs() as u32;
        let height = ((self.0).1 - (self.1).1).abs() as u32;

        width * height
    }
}

fn main() {
    let left_up_in = Point(-1, 1, 1);
    let right = Point(1, 0, 0);
    println!("Dist is: {}.", right.euclidian_dist(&left_up_in));

    let my_rect = Rect(left_up_in, right);
    println!("Area is: {}", my_rect.area());

    println!("The Rectangle is {:?}", my_rect);
}
