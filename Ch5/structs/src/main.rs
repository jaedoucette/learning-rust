struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn main() {
    let first_user = User {
        email: String::from("someuser@awebsite.com"),
        active: false,
        sign_in_count: 128,
        username: String::from("someuser"),
    };

    let second_user = build_user(String::from("su@web.com"), String::from("su"));

    println!("User 1's email was: {}", first_user.email);
    println!("User 2's email was: {}", second_user.email);


    let third_user = User {
        email : String::from("tu@web.com"),
        username: String::from("tu"),
        ..second_user
    };
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        sign_in_count: 0,
        active: false,
    }
}
