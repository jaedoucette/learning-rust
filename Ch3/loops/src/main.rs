fn main() {
    infinite_loops();
    while_loops();
    for_loops();
}

fn while_loops() {
    let mut number_loops = 0;
    while number_loops <= 1000 {
        number_loops = number_loops + 1;
    }
    println!("While-looped {} times!", number_loops);
}

fn infinite_loops() {
    let mut number_loops = 0;
    loop {
        number_loops = number_loops + 1;
        if number_loops >= 100 {
            break;
        }
    }
    println!("Looped {} times.", number_loops);
}

fn for_loops() {
    let letters = ['h', 'e', 'l', 'o', 'w', 'r', 'd', ' '];

    for element in letters.iter() {
        println!("{}", element);
        match element {
            'l' => println!("{}", element),
            'o' => println!("{}", " "),
            'w' => println!("{}", "o"),
            'r' => println!("{}", "l"),
            _ => continue,
        }
    }
}
