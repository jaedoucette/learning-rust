fn main() {
    let x = 5;
    println!("The value of x is: {}", x);
    let x = 6;
    println!("The value of x is: {}", x);

    const UNCHANGABLE : u32 = 5;
    println!("Twice UNCHANGABLE is {}.", 2*UNCHANGABLE);
    
    let integer_set = 'ℤ'; 
    let my_tupple: (i32, u32, char) = (x, UNCHANGABLE, integer_set);
    let (a, b, c) = my_tupple;
    println!("Tupple's third element was {}", c);

    println!("Tupple's second element was {}", my_tupple.1);

    let a = ['a', 'b', 'c', 'd', 'e'];
    let first = a[0];
    println!("The first element is {}", first);
}

