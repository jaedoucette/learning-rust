
fn main() {
    let gifts = ["Two Turtle Doves", "Three French Hens", "Four Calling Birds",
        "!!!FIVE! GOLDEN! RINGS!!!!", "Six Geese a-Laying",
        "Seven Swans a-Swimming", "Eight Maids a-Milking", 
        "Nine Ladies Dancing", "Ten Lords a-Leaping", "Eleven Pipers Piping",
        "Twelve Drummers Drumming"];
    let days = 1..13;

    for day in days {
        print_day(day, &gifts[0..(day-1)]);
        println!("\n---------------------\n");
    }
}

fn print_day(n : usize, gifts : &[&str]) {
    let gifts = gifts.iter();
    let suffix = match n {
        1 => "st",
        2 => "nd",
        3 => "rd",
        _ => "th",
    };
    println!("On the {}{} day of Christmas, my true love gave to me...", 
             n, suffix);

    for gift in gifts.rev() {
        println!("{},", gift);
    }

    match n {
        1 => println!("A partridge in a pear tree!"),
        _ => println!("And a partridge in a pear tree!"),
    }
}
