use std::io;

fn main() {
    loop {
        println!("Enter f for fahr -> cel, or c for cel -> fahr:");
        let mut buf = String::new();
        io::stdin().read_line(&mut buf).expect("Can't read line...");
        let buf = buf.trim();

        match buf {
            "c" => println!("{}", celsius_to_fahrenheit(read_number())),
            "f" => println!("{}", fahrenheit_to_celsius(read_number())),
            "n" => break,
            _ => {
                println!("Invalid mode... try again or enter [n] to quit.");
                continue;
            }
        }
    }
}

fn read_number() -> f32 {
    let mut res = 0.0;
    loop {
        println!("Enter a temperature to convert:");
        let mut line = String::new();
        let parsed = match io::stdin().read_line(&mut line) {
            Err(_) => {
                println!("Bad input!");
                continue;
            },
            Ok(_) => line.trim().parse(),
        };

        match parsed {
            Ok(num) => {
                res = num;
                break;
            }
            Err(_) => println!("Not a number?"),
        }
    }
    res
}


fn fahrenheit_to_celsius(t : f32) -> f32 {
    (t - 32.0)/1.8
}

fn celsius_to_fahrenheit(t : f32) -> f32 {
    t * 1.8 + 32.0
}

