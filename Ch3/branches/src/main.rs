fn main() {
    let branching_num = 1;

    let result = if branching_num > 3 {
        println!("{} is greater than 3.", branching_num);
        1
    } else if branching_num % 2 == 0 {
        println!("{} is less than or equal to 3, and even.", branching_num);
        2
    } else {
        println!("{} is less than or equal to 3, and odd.", branching_num);
        3
    };

    println!("Passed through arm #{}.", result);

}
