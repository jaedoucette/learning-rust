fn main() {
    println!("Hello, world!");
    let result = second_function(5);
    println!("The result of second_function was {}", result);
}

fn second_function(x: i32) -> i32 {
    println!("I was called with argument {}!", x);
    x + 1
}
