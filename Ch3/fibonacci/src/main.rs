use std::io;

fn main() {
    let mut line = String::new();

    loop {
        println!("Enter desired element of Fib sequence:");
        io::stdin().read_line(&mut line).expect("Can't readline?");

        let n = line.trim().parse();

        match n {
            Ok(num) => {
                println!("{}", fib(num));
                println!("{}", slow_fib(num));
                break;
            }
            Err(_) => println!("Not a number, try again."),
        }
    }
}

fn fib(n: u8) -> u64 {
    //Don't know extensible data structures yet in this
    //language, so I can't memoize easily. Writing ugly
    //but fast version over nice but slow.

    let mut old_fib = 1; //fib(0) = 1
    let mut fib = 1; //fib(1) = 1
    for _i in 1..n {
        fib = fib + old_fib;
        old_fib = fib - old_fib;
    }

    fib
}

fn slow_fib(n: u8) -> u64 {
    //Wrote slow fib for fun too.
    match n {
        0 => 1,
        1 => 1,
        _ => slow_fib(n - 1) + slow_fib(n - 2),
    }
}
