use std::io;
use std::collections::HashMap;

fn main() {
    let mut inputs = read_inputs();
    println!("Read: {:?}", inputs);
    println!("Average is : {}", mean(&inputs));
    println!("Median is : {}", median(&mut inputs));
    println!("Mode is : {:?}", mode(&mut inputs));
}


//Computes the mean (average) of a vector of 32-bit integers.
//Input: numbers: a vector of 32 bit integers.
//Output: The average of the elements of numbers.
fn mean(numbers : &Vec<i32>) -> f64 {
    let mut count = 0;
    for n in numbers {
        count += n;
    }

    match numbers.len() {
        0 => 0.0,
        _ => count as f64 / numbers.len() as f64
    }
}

//Computes the median of a vector of 32-bit integers, 
// sorting the vector as a side effect.
//Input: numbers: a mutable referece to a vector of 32-bit integers.
//Output: The median of the elements of numbers.
//Post-Condition: numbers has been sorted.
fn median(numbers : &mut Vec<i32>) -> f64 {
    //Perform an insertion sort.
    //TODO: replace this with a libarary sort once I learn syntax.
    //Or, beter, replace with the linear time algorithm.`
    for i in 0..numbers.len() {
        for j in (1..i+1).rev() {
            match numbers[j] < numbers[j-1] {
                true => { let temp = numbers[j-1];
                    numbers[j-1] = numbers[j];
                    numbers[j] = temp;
                },
                false => break,
            }   
        }
    }

    let median_index = numbers.len() / 2;

    match numbers.len(){
        0 => 0.0,
        _ => match numbers.len() % 2 {
            1 => numbers[median_index] as f64,
            _ => ((numbers[median_index-1] + numbers[median_index]) as f64)/2.0
        }
    }
}

//Computes the mode(s) of a vector of 32-bit integers.
//Input: numbers: a reference to a vector of 32-bit integers.
//Output: A vector containing all elements of numbers that occur most
//  frequently. A vector is needed to handle the case of a tie. In 
//  case of an empty input vector, and empty output is produced.
fn mode(numbers: &Vec<i32>) -> Vec<i32> {
    let mut occurences = HashMap::new();
    
    for i in numbers {
        let old_count = occurences.entry(i).or_insert(0);
        *old_count += 1;
    }

    let mut maximum = match numbers.len() > 0 {
        true => *(occurences.get(&numbers[0]).unwrap()),
        false => 0,
    };

    for (k, v) in &occurences {
         match *v > maximum {
            true => maximum = *v,
            false => continue,
         }
    }

    let mut result = Vec::new();
    for (k, v) in &occurences {
        match *v == maximum {
            true => result.push(**k),
            false => continue,
        }
    }

    result
}


fn read_inputs() -> Vec<i32>{
    let mut result = Vec::new();
    println!("Enter numbers, one per line. Send EOF when complete.");
    loop {
        let mut next_line  = String::new();
        match io::stdin().read_line(&mut next_line) {
            Err(_error) => println!("Failed read."),
            Ok(0) => break,
            Ok(_n) => (),
        }

        let next_int : i32 = match next_line.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        result.push(next_int);
    }
    result
}
