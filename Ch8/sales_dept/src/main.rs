use std::collections::HashMap;
use std::io;

struct AddCommandData {
    name: String,
    department: String,
}

struct ListingCommandData {
    department: String,
}

struct InvalidCommandData {
    input: String,
}

enum CommandData {
    AddType(AddCommandData),
    ListingType(ListingCommandData),
    InvalidType(InvalidCommandData),
}

fn main() {
    let mut database = HashMap::new();

    loop {
        let command = parse_command();

        let result = match command {
            CommandData::AddType(cmd) => insert(cmd, &mut database),
            CommandData::ListingType(cmd) => retrieve(cmd, &database),
            CommandData::InvalidType(_cmd) => String::from("Invalid Input: "),
        };

        println!("{}", result);
    }
}

fn retrieve(c: ListingCommandData, database: &HashMap<String, Vec<String>>) -> String {
    if database.get(&c.department) == None {
        return String::from("No Such Department");
    }

    let mut result = String::new();

    for person in database.get(&c.department).unwrap() {
        result.push_str(&format!("{}, ", person));
    }

    result
}

fn insert(c: AddCommandData, database: &mut HashMap<String, Vec<String>>) -> String {
    let department_listing = database.entry(c.department.clone()).or_insert(Vec::new());

    let mut found = false;
    for person in department_listing.iter() {
        found = *person == c.name;
        if found {
            break;
        }
    }

    let result = match found {
        true => format!(
            "Warning: {} is already in {}, so adding a copy.",
            &c.name, &c.department
            ),
        false => String::from("Inserted successfully."),
    };
    (*department_listing).push(c.name);
    result
}

fn parse_command() -> CommandData {
    let mut line = String::new();
    let _bytes_read = io::stdin().read_line(&mut line);

    let mut tokens = line.split_whitespace();
    match tokens.next() {
        Some("Add") =>  parse_add(tokens),
        Some("List") => parse_listing(tokens),
        _ => CommandData::InvalidType(InvalidCommandData {
            input: String::from("Invalid Commmand."),
        }),
    }
}

fn parse_listing(mut tokens: std::str::SplitWhitespace) -> CommandData {
    let department = tokens.next();

    match department != None && tokens.next() == None {
        true => CommandData::ListingType(ListingCommandData {
            department: String::from(department.unwrap()),
        }),
        false => CommandData::InvalidType(InvalidCommandData {
            input: String::from("Invalid CommandData!"),
        }),
    }
}

fn parse_add(mut tokens: std::str::SplitWhitespace) -> CommandData {
    let name = tokens.next();
    let department = tokens.next();

    match name != None && department != None && tokens.next() == None {
        true => CommandData::AddType(AddCommandData {
            name: String::from(name.unwrap()),
            department: String::from(department.unwrap()),
        }),
        false => CommandData::InvalidType(InvalidCommandData {
            input: String::from("Invalid CommandData!"),
        }),
    }
}
