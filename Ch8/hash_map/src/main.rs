use std::collections::HashMap;

fn main() {
    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    let mut v1 = vec!["A", "B", "C"];
    let v2 = vec!['a', 'b', 'c'];
    let collected : HashMap<_, _> = v1.iter().zip(v2.iter()).collect(); 

    println!("A lowercase \"A\" is {}", collected.get(&"A").unwrap());


    for (k, v) in &collected {
        println!("{} => {}", k, v);
    }

    //entry produces either a valid reference, OR a 
    // sort of "D"/None pair. or_insert does insertion
    // on "D" only if the D/None pair is produced.
    {
        let ref_to_value = scores.entry(&"D").or_insert('d'); 
        *ref_to_value = 'e'
    }


}
