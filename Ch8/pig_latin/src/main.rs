
use std::io;

fn main() {

    loop {
        println!("Enter some text");
        let mut line = String::new();
        let result = io::stdin().read_line(&mut line);

        match result {
            Err(_e) => continue,
            Ok(n) => match n {
                0 => break,
                _ => (),
            },
        }

        let mut result = String::new();
        let mut outside_word = true;
        let mut last_starting_letter = ' ';
        line.push(' '); //trialing whitespace to trigger the middle condition.
        for c in line.chars() {
            if outside_word && !c.is_whitespace() {
                outside_word = false;
                last_starting_letter = match c{
                    'a' | 'e' |  'i' |  'o' | 'u' => 'h',
                    _ => c,
                };
            }
            else if !outside_word && c.is_whitespace() {
                result.push(last_starting_letter);
                result.push_str("ay");
                result.push(c);
                outside_word = true;
            }
            else{
                result.push(c);
            }
        }

        result.pop(); //remove the trailing space we added above.

        println!("{}", result);
    }

}
