fn main() {
    let mut v: Vec<i32> = Vec::new();

    //This is a useful macro.
    let u = vec!["Hello", "World"];

    v.push(3);
    v.push(1);
    v.push(4);
    v.push(1);
    v.push(5);
    v.push(9);

    v[0] = 5;
    {
        let fifth_element = &v[5];
        let fifth_element_optional = v.get(2);

        let not_an_element = v.get(100);

        match not_an_element {
            Some(&i) => println!("Strangely, returned something past vec end."),
            None => println!("As expected, nothing past vec end."),
        }
    }

    for elem in &mut v {
        println!("{}", elem);
        *elem += 1;
        println!("{}", elem);
    }

}
