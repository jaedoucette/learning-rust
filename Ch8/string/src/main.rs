fn main() {
    let mut my_string = String::new();

    my_string = "Hello World".to_string();

    println!("{}", my_string);

    my_string.push_str(" is a good string.");

    println!("{}", my_string);

    let summed_string = "Hello".to_string() + &" ".to_string()+ "World";

    let formatted_string = format!("{} {}", "Hello", "World");

    println!("{} {}", summed_string, formatted_string);

    for c in "A longer string, possibly with multi-byte chars...".chars(){
        println!("{}", c);
    }
}
