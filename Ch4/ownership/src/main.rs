fn main() {
    let mut s = String::from("First String.");

    if true {
        let s = String::from("Hello World");
    } else {
        let s = String::from("Hello World2");
    }

    s.push_str(" With some stuff appended...");
    println!("{}", s);

    take_ownership_of_string(s);

    s = return_ownership_of_string(String::from("There and Back"));
    println!("{}", s);

}

fn return_ownership_of_string(s : String) -> String {
    return s;
}

fn take_ownership_of_string(s : String){
    println!("\"{}\" is mine now.", s);
}
