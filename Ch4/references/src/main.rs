fn main() {
    let s = String::from("xavier");
    println!("{} has {} x's.", s, count_x(&s));
}

fn count_x(s: &String) -> u32 {
    let mut x_count = 0;
    for i in s.chars() {
        if i == 'x' {
            x_count = x_count + 1;
        }
    }
    x_count
}
